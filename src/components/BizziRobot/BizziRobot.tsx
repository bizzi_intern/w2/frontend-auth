import classNames from 'classnames/bind';
import styles from './BizziRobot.module.scss';

const cx = classNames.bind(styles);

function BizziRobot() {
    return ( 
        <div>
            <img className={cx("robot")} src="./bizzi-robot.svg" alt="Bizzi Robot" />
        </div>
    );
}

export default BizziRobot;